#Default compose args
COMPOSE_CMD="docker-compose -f jenkins.yml -p jenkins "

#Make sure old containers are gone
sudo $COMPOSE_CMD stop
sudo $COMPOSE_CMD rm --force -v

#build the system
sudo $COMPOSE_CMD build --no-cache
sudo $COMPOSE_CMD up -d

#run unit tests
sudo $compos_cmd run --no-deps --rm -e ENV=UNIT identidock
ERR=$?

#Run system test if unit tests passed
if [ $ERR -eq 0 ]; then
	IP=$(sudo docker inspect -f {{.NetworkSettings.IPAddress}} jenkins_identidock_1)
	CODE=$(curl -sL -w "%{http_code}" $IP:9090/monster/bla -o /dev/null) || true

	if [ $CODE -ne 200 ]; then
		echo "Site returned " $CODE
		ERR=1
	fi
fi
#Pull down the system
sudo $COMPOSE_CMD stop
sudo $COMPOSE_CMD rm --force -v
return $ERR
