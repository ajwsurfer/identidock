imageIds=$( docker images | grep '<none>' |  awk '{ print $3 }')
if [ -z "$imageIds" ]; then
  echo 'No extra images'	
else 
  for var in "${imageIds[@]}"
  do
     printf 'Removing unused image id: %s ...\n' ${var}
     docker rmi ${var}

    # do something on $var
  done
fi 
