#!/usr/bin/env bash
echo "stopping container..."
docker stop identidock 
echo "removing container..."
docker rm identidock
echo "building image..."
docker build -t identidock .
echo "building and running container..."
#docker run -d -p 9090:9090 -p 9191:9191 -v "$(pwd)"/app:/app --name identidock identidock
docker run -e "ENV=DEV" -p 5000:5000 --name identidock identidock

echo "2 second delay..."
sleep 2

echo "checking container logs..."
docker logs identidock
echo "testing opperation of container..."
#curl localhost:9090
curl localhost:5000
echo "test ports..."
docker port identidock



